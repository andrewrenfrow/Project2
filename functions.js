
/*
	This function examines the first and last name of each item
	in the profs array, as well as a concatenation of the two,
	and if a match is found returns the object (prof) stored
	in that array index.
*/

function searchProfDB(input) {
	tmp = input;
	input = input.toLowerCase();
	for (var i = 0; i < profs.length; i++) {
		if (profs[i].firstName.toLowerCase() == input || profs[i].lastName.toLowerCase() == input || (profs[i].firstName.toLowerCase() + " " + profs[i].lastName.toLowerCase()) == input) {
			updateDataSheet(profs[i]);
			document.getElementById("firstName").innerHTML = "First Name: " + profs[i].firstName;
			document.getElementById("lastName").innerHTML = "Last Name: " + profs[i].lastName;
			document.getElementById("faculty").innerHTML = "Faculty: " + profs[i].faculty;
			document.getElementById("department").innerHTML = "Department: " + profs[i].department;
			document.getElementById("profession").innerHTML = "Profession: " + profs[i].profession;
			document.getElementById("office").innerHTML = "Office: " + profs[i].office;
			document.getElementById("phone").innerHTML = "Phone: " + profs[i].phone;
			document.getElementById("email").innerHTML = "Email: " + profs[i].email;
		}
	}
}


/*
	This function examines the course code in the courses array,
	and if a match is found returns the object (course) stored in
	that array index.
*/

function searchCoursesDB(input){
	for (var i = 0; i < courses.length; i++) {
		if ((courses[i].depCode.toLowerCase() + " " + courses[i].courseNum) == input ||
            (courses[i].depCode.toLowerCase() + courses[i].courseNum) == input || courses[i].name.toLowerCase() == input) {
			if (debug == true) {
				alert("found it!");
			}
			var searchResult = courses[i];
			if(debug == true) {
				alert(searchResult.depCode);
			}
			return searchResult;
		}
	}
	if (debug == true) {
		alert("Made it to the end of the array. If what you were looking for was in there, this code didn't work");
	}
	return;
}



/*
	This function updates the HTML Course Info fields on the CoursesSecondary page with the results
	from searchCoursesDB function.
*/

function displayCourseSearch(input){
	if(debug == true){
		console.log(input.depCode);
		console.log(input.courseNum)
		console.log(input.name);
		console.log(input.sections);
	}

	document.getElementById("code").innerHTML = "Course Code: " + input.depCode + " " + input.courseNum;
	document.getElementById("name").innerHTML = "Course Name: " + input.name;
	var sectionsConcat = "Sections: ";
	for (var i = 0; i < input.sections.length; i++){
		sectionsConcat = sectionsConcat + "A0" + (i+1) + ": " + input.sections[i];
		if(i != input.sections.length - 1) {
			sectionsConcat = sectionsConcat + " /// ";
		}
	}
	document.getElementById("sections").innerHTML = sectionsConcat;
	var instructorsConcat = "Taught by: ";
	for (var i = 0; i < input.instructors.length; i++){
		instructorsConcat = instructorsConcat + input.instructors[i].firstName + " " + input.instructors[i].lastName;
		if(i != input.instructors.length - 1) {
			instructorsConcat = instructorsConcat + " /// ";
		}
	}
	document.getElementById("courseProfs").innerHTML = instructorsConcat;
	return false;
}


function readDataSheet() {
	$.ajax({
  // http may be used instead of https if required
  url: "https://sheetlabs.com/PTAJ/RWDataset",
  crossDomain : true,
  beforeSend: function(xhr, settings) {
    xhr.setRequestHeader("Authorization","Basic "+btoa("jesuisahmedn@gmail.com:t_b6b3dd6f8b5a2be24dbd606ab475dfa3"));
  }
})
.done(function(data) {
  if (data.length == 0) {
    console.log("No results found");
    return;
  }
  $.each(data, function(key, value) {
     console.log(value);
  });
})
.fail(function() {
  console.log("Failed");
});
}

function updateDataSheet(input) {
	$.ajax({
  // http may be used instead of https if required
  url: "https://sheetlabs.com/PTAJ/RWDataset",
  crossDomain : true,
  beforeSend: function(xhr, settings) {
    xhr.setRequestHeader("Authorization","Basic "+btoa("jesuisahmedn@gmail.com:t_b6b3dd6f8b5a2be24dbd606ab475dfa3"));
  }
})
.done(function(data) {
  if (data.length == 0) {
    console.log("No results found");
    return;
  }
  $.each(data, function(key, value) {
		var fullName = value.professors.toLowerCase();
		var timessearched = value.timessearched;
		var space = fullName.indexOf(" ");
		var firstName = fullName.substring(0, space);
		var LastName = fullName.substring(space);
     if (fullName == input.fullName.toLowerCase() || firstName == input.firstName.toLowerCase()) {
			 var URL = "https://api.sheetlabs.com/records/PTAJ/3456" + "/" + input.id;
			 $.ajax({
		 		type: "put",
		 		dataType: "json",
		 		data: JSON.stringify( { "professors": value.professors, "timessearched": timessearched+1 } ),
		 		url: URL,
		 		success: function( data, textStatus, jQxhr ){
		       	console.log( JSON.stringify( data ) );
		     },
		     error: function( jqXhr, textStatus, errorThrown ){
		         console.log( errorThrown );
		     },
		 		beforeSend: function(xhr, settings) {
		 	    xhr.setRequestHeader("Authorization","Basic "+btoa("jesuisahmedn@gmail.com:t_b6b3dd6f8b5a2be24dbd606ab475dfa3"));
		 	  }
		 	})
		 }
  });
})
.fail(function() {
  console.log("Failed");
});
/*
	$.ajax({
		type: "post",
		dataType: "json",
		data: JSON.stringify( { "professors": "Zach Thompson", "timessearched": "7" } ),
		url: "https://api.sheetlabs.com/records/PTAJ/3456",
		success: function( data, textStatus, jQxhr ){
      	console.log( JSON.stringify( data ) );
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
    },
		beforeSend: function(xhr, settings) {
	    xhr.setRequestHeader("Authorization","Basic "+btoa("jesuisahmedn@gmail.com:t_b6b3dd6f8b5a2be24dbd606ab475dfa3"));
	  }
	})
*/

}
