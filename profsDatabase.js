/*
	This JS file is a sample database of professors teaching
	in the department of computer science at UVic this
	semester. This database is accessed  by the
	searchProfsDB() function in the functions.js file.
*/

var wBird = {
	fullName: "William Bird",
	firstName: "William",
	lastName: "Bird",
	faculty: "Engineering",
	department: "CSC",
	profession: "Professor (Limited Term)",
    office: "ECS526",
	phone: "250-472-5833",
	email: "bbird@uvic.ca",
	id:"4"
};

var yCoady = {
	fullName: "Yvonne Coady",
	firstName: "Yvonne",
	lastName: "Coady",
	faculty: "Engineering",
	department: "CSC",
	profession: "Director of (Student) Outreach, Professor",
    office: "ECS610",
	phone: "250-472-5715",
	email: "ycoady@uvic.ca",
	id:"2"
};

var tRooij = {
	fullName: "Tibor Rooij",
	firstName: "Tibor",
	lastName: "Rooij",
	faculty: "Engineering",
	department: "CSC",
	profession: "Assistant Teaching Professor (Limited Term)",
	office: "ECS253",
	phone: "N/A",
	email: "vanrooij@uvic.ca",
	id:"8"
};

var jWeber = {
	fullName: "Jens Weber",
	firstName: "Jens",
	lastName: "Weber",
	faculty: "Engineering",
	department: "CSC",
	profession: "Combined Program Advisor-Health Information Science/CSC, Professor",
	office: "ECS568",
	phone: "250-472-5758",
	email: "jens@uvic.ca",
	id:"10"
};

var bBultena = {
	fullName: "Bette Bultena",
	firstName: "Bette",
	lastName: "Bultena",
	faculty: "Engineering",
	department: "CSC",
	profession: "Senior Lab Instructor",
	office: "ECS322",
	phone: "250-472-5853",
	email: "bbultena@csc.uvic.ca",
	id:"12"
};

var mCheng = {
	fullName: "Mantis Cheng",
	firstName: "Mantis",
	lastName: "Cheng",
	faculty: "Engineering",
	department: "CSC",
	profession: "Assistant Professor",
	office:"ECS632",
	phone:"250-472-5737",
	email: "mcheng@uvic.ca",
	id:"14"
};

var rLittle = {
	fullName: "Rich Little",
	firstName: "Rich",
	lastName: "Little",
	faculty: "Engineering",
	department: "CSC",
	profession: "Assistant Teaching Professor (Limited Term)",
	office:"ECS516",
	phone:"250-472-5752",
	email: "rlittle@uvic.ca",
	id:"16"
};

var tWei = {
	fullName: "Tianming Wei",
	firstName: "Tianming",
	lastName: "Wei",
	faculty: "Engineering",
	department: "CSC",
	profession: "Lab Instructor, Phd Student",
	office: "ECS452",
	phone:"250-472-5863",
	email: "twei@uvic.ca",
	id:"18"
};

var lJackson = {
	fullName: "Lillanne Jackson",
	firstName: "Lillanne",
	lastName: "Jackson",
	faculty: "Engineering",
	department: "CSC",
	profession: "Associate Dean Undergraduate Studies, Professor",
    office: "ECS253",
    phone: "N/A",
    email: "lillanne@csc.uvic.ca",
	id:"20"
};

var mZastre = {
	fullName: "Michael Zastre",
	firstName: "Michael",
	lastName: "Zastre",
	faculty: "Engineering",
	department: "CSC",
	profession: "Professor",
	office: "ECS528",
	phone: "250-472-5771",
	email: "zastre@cs.uvic.ca",
	id:"22"
};

var hMuller = {
	fullName: "Hausi Muller",
	firstName:"Hausi",
	lastName:"Muller",
	faculty: "Engineering",
	department: "CSC",
	profession: "Professor",
	office: "ECS614",
	phone: "250-472-5719",
	email: "hausimuller@gmail.com",
	id:"6"
};


var profs = [wBird, yCoady, tRooij, jWeber, bBultena, mCheng, rLittle, tWei, lJackson, hMuller];
