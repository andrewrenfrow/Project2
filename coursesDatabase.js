/*
	This JS file is a sample database of first year computer science
	courses offered at UVic this semester. This database is accessed
	by the searchCoursesDB() function in the functions.js file.
*/

var csc100 = {
	name:"Elementary Computing",
	depCode:"CSC",
	courseNum:"100",
	numSections: "1",
	sections:[a01 = "M,W,Th  2:30 pm-3:20p m"],
	instructors:[mCheng],
};

var csc105 = {
	name:"Computers and Information Processing",
	depCode:"CSC",
	courseNum:"105",
	numSections: "2",
	sections:[a01 = "M,W,Th  2:30 pm-3:20 pm", a02 = "T,W,F  08:30 am-09:20 am"],
	instructors:[rLittle, tWei],
};

var csc106 = {
	name:"The Practice of Computer Science",
	depCode:"CSC",
	courseNum:"106",
	numSections: "2",
	sections:[a01 = "T,W,F	11:30 am-12:20 pm", a02 = "T,W,F	11:30 am-12:20 pm"],
	instructors:[tRooij],
};

var csc110 = {
	name:"Fundamentals of Programming I",
	depCode:"CSC",
	courseNum:"110",
	numSections: "2",
	sections:[a01 = "M,Th  10:00 am-11:20 am", a02 = "M,Th  10:00 am-11:20 am"],
	instructors:[lJackson],
};

var csc111 = {
	name:"Fundamentals of Programming with Engineering Applications",
	depCode:"CSC",
	courseNum:"111",
	numSections: "2",
	sections:[a01 = "M,Th  10:00 am-11:20 am", a02 = "M,Th  10:00 am-11:20 am"],
	instructors:[wBird],
};

var csc115 = {
	name:"Fundamentals of Programming II",
	depCode:"CSC",
	courseNum:"115",
	numSections: "4",
	a01:"M,Th	11:30 am-12:50 pm",
	a02:"M,Th	11:30 am-12:50 pm",
	a03:"T,W,F  02:30 pm-03:20 pm",
	a04:"T,W,F	02:30 pm-03:20 pm",
	sections:[a01 = "M,Th	11:30 am-12:50 pm", a02 = "M,Th	11:30 am-12:50 pm",
	a03 = "T,W,F  02:30 pm-03:20 pm", a04 = "T,W,F  02:30 pm-03:20 pm"],
	instructors:[mZastre,tRooij],
};

var csc130 = {
	name:"World Wide Web and Mobile Applications",
	depCode:"CSC",
	courseNum:"130",
	numSections: "1",
	sections:[a01 = "M,Th  01:00 pm-02:20 pm"],
	instructors:[yCoady],
};

var courses = [csc100, csc105, csc106, csc110, csc111, csc115, csc130];
